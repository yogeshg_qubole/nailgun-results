1. tar -xf rubygems-2.7.6.tar 
   ruby rubygems-2.7.6/setup.rb
2. Update Java version:
  a) sudo update-alternatives --display java
  b) sudo update-alternatives --config java
3. run make && sudo make install from nailgun
4. for railgun, run:
	a) gem build railgun.gemspec
        b) gem install railgun-0.0.1.gem
5. for ng:
  a) start server java -cp `hadoop classpath`:/home/ec2-user/nailgun/nailgun-server/target/nailgun-server-0.9.3-SNAPSHOT.jar com.martiansoftware.nailgun.NGServer
  b) ng --nailgun-port 2113 ng-alias fs org.apache.hadoop.fs.FsShell : to make fs command an alias
  c) ng --nailgun-port 2113 class_name : to run
6. Configure the aws keys in core-site.xml file before trying to use any of the scripts

