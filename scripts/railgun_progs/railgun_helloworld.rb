require 'railgun'

client = Railgun::Client.new

client.connect
args = %w{ -ls s3://chintanb-upload-test/ }
result = client.execute('fs', args: args)
client.close

puts result.out
puts result.err
puts result.exitcode
