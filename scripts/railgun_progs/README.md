This directory contains 3 files:
1. railgun_helloworld : A basic introduction to railgun with the gem used and some idea <br>of how to use connections and print results for a hardcoded url
2. railgun_uploads : Builds on the previous example and performs not only the operations of upload but also measures the time taken
3. em_railgun_upload : An attempt to wrap the railgun code in a EM to allow for retries and error handling, not functional as of now.
