from datetime import datetime
import sys
end_points_file=sys.argv[1]
start_map={}
end_map={}

filename = end_points_file
file = open(filename, "r")

total_time=0.0
cnt=0
last_time=0
for line in file:
   cnt+=1
   data=line.strip().split(" ")
   time=data[-1]
   mm,ss=time.strip().split("m")
   ss=ss.strip().split("s")[0]
   total_time+=last_time
   last_time=float(mm)*60+float(ss)
file.close()
print  "%d %f %f"%(cnt-1,total_time,total_time/(cnt-1))
