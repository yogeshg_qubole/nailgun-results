require 'railgun'
src=ARGV[0]
dest=ARGV[1]
client = Railgun::Client.new
args= "-put -f #{src} #{dest}".split(' ')
start=Time.now()
client.connect
result = client.execute('fs', args: args)
puts result.out
puts result.err
puts result.exitcode

client.close
print Time.now()-start
