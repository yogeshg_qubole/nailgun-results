=begin

Task: Running a simple fog command to upload a file to s3.
Methodology:
1.Configure AWS access and secret keys to env variables
2.Create a connection to aws using these creds
3.Connect to a bucket and the directory path to upload
4.Perform upload command

We Use Time.now betweem step 2 and step 4 only to get accurate measurement of time,
excluding the ruby start time and gem loading time.

=end
require 'fog-aws'
accesskey=ENV['AWS_ACCESS_KEY']
secretkey= ENV['AWS_SECRET_KEY']
src=ARGV[0]
dest=ARGV[1]
start=Time.now()
connection = Fog::Storage.new({
  :provider                 => 'AWS',
  :aws_access_key_id        => accesskey,
  :aws_secret_access_key    => secretkey,
  :region => 'us-east-1'
})
# get the bucket
s3_bucket_name = 'chintanb-upload-test'
dir =   connection.directories.detect { | dir | dir.key == s3_bucket_name }
filename="#{dest}"
file = dir.files.create(
  :key    => filename,
  :body   => File.open("#{src}"),
  :public => true
)
puts "#{dest} #{Time.now()-start}"
