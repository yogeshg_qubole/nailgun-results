from datetime import datetime
import sys
def to_mil(s):
    hh,mm,ss=s.split(":")
    ss,ms=ss.split(".")
    return float(ss)+float("0."+ms)
start_points_file=sys.argv[2]
end_points_file=sys.argv[1]

print sys.argv

start_map={}
end_map={}

filename = end_points_file
file = open(filename, "r")

for line in file:
   data=line.strip().split(" ")
   time=data[-1]
   fname=data[0].split("/")[-1]
   end_map[fname]=time
file.close()
max_time=0
filename = start_points_file
file = open(filename, "r")
for line in file:
   fname,time=line.strip().split(" ")
   fname=fname.split("/")[-1]
   print fname,time,end_map[fname]
   end_time = end_map[fname]
   start_time = time
   end_FMT = '%H:%M:%S,%f'
   start_FMT='%H:%M:%S.%f'
   run_time = to_mil(str(datetime.strptime(end_time, end_FMT) - datetime.strptime(start_time,start_FMT)))
   print run_time
   max_time=max(max_time,run_time)
if max_time>=0.0:
    print "Maximum upload time for a single file was: ",max_time,"seconds" 
else:
    print "Maximum upload time for a single file was: <1 second" 
file.close()

