require 'eventmachine'
require 'railgun'

def upload(src,dest)
client = Railgun::Client.new
args= "-put -f #{src} #{dest}".split(' ')
client.connect
result = client.execute('fs', args: args)
#puts result.out
#puts result.err
result.exitcode
client.close
end

def run_command(src,dest)
EM.run do
op = proc do
upload(src,dest)
end

callback = proc do |retcode| 
puts "returned code #{retcode}" 
EM.stop
end
errorback = proc do |retcode|
puts "faced error code #{retcode} "
puts "RETRY file #{dest}"
upload(src,dest)
EM.stop
end
EM.defer(op, callback,errorback)

end
end
src=ARGV[0]
dest=ARGV[1]
port=ARGV[2]

run_command(src,dest)
